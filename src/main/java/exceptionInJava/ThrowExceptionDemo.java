package exceptionInJava;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;

/**
 * @author: nvankhoiws@gmail.com
 * @since: 23/10/2021
 */
public class ThrowExceptionDemo {
    public void writeFile() throws FileNotFoundException { // báo cho các method mẹ gọi đến nó biết việc nó có khả năng phun ra những exception gì
        // Case 1. Chạy nuột
        // Case 2. Có lỗi
        // phun ra exception
    }

    /**
     * download ont file from internet
     */
    public void downloadFile() throws ConnectException { // hàm này có khả năng phun ra IOException
        // Case 1. Chạy nuột
        // Case 2. Có lỗi
    }
}
