package finalexam;

import java.util.Date;

public class Visit {

    private Customer customer;

//    private java.util.Date date; // full package import
    private Date date; // full package import

    private double serviceExpense;
    private double productExpense;

    public Visit(String name, Date date) {
        this.customer = new Customer(name);
        this.date = date;
    }

    public String getName() {
        return this.customer.getName();
    }

    /**
     * Tính giá trị chi tiêu hóa đơn gồm cả membership
     * @return
     */
    public double getServiceExpense() {
        if (!this.customer.isMember()) {
            return serviceExpense;
        } else {
            if (this.customer.getMemberType().equals("Premium")) {
                return serviceExpense * 0.8; // discount 20%
            }
            if (this.customer.getMemberType().equals("Gold")) {
                return serviceExpense * 0.85; // discount 15%
            }
            if (this.customer.getMemberType().equals("Silver")) {
                return serviceExpense * 0.90; // discount 10%
            }
        }
//        if (this.customer.getMemberType().equals("None")) // C1
//            return serviceExpense*1; // discount 0%
        return serviceExpense*1; // discount 0%
    }

    /**
     * Chi tiêu thêm chứ không phải setter
     * @param serviceExpense
     */
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = this.serviceExpense + serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        return getServiceExpense() + getProductExpense();
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "customer=" + customer.getName() +
                ", date=" + date +
                ", serviceExpense=" + serviceExpense +
                ", productExpense=" + productExpense +
                '}';
    }
}
