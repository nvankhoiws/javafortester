DDL- Data Definition Language
Các câu lệnh gây ra biến đổi database: create database, create table, grant quyền

DML - Data Manipulation Languate
select, update, delete

Một statement trong sql là một câu lệnh:
create table testtable (id bigint primary key auto_increment, name varchar(255))


// Exercise on class - timebox 10 phút: 11h15 --> 11h30
Tạo một class test gọi vào method select table
In ra từng giá trị của mỗi column trong table đó

=======================================================================
// Bài tập về nhà
Sử dụng jdbc:
- Tạo ra một bảng tên là: BankingAccount có các column như sau:
  + id: bigint // primary key theo kiểu tự tăng dần
  + account_name: varchar(50) // tên tài khoản
  + balance: float // số dư

- Trong table có ít nhất 2 account (2 records)

- Thực hiện một giao dịch chuyển khoản từ account 1 sang account 2 (nghiệp vụ này có read và update)

- In ra balance của 2 account sau khi thực hiện xong giao dịch chuyển khoản


=============
Course-Student-Enronment
1. Khi tạo Enrollment: chú ý việc duplicate enrollment
2. Khi read: từ enrollment: no problems
3. khi update: 
   update student --> không ảnh hưởng,update last name, firstname
   update course ---> không ảnh hưởng
   update enrollment -->
   
4. delete student
    phải delete enrollment


