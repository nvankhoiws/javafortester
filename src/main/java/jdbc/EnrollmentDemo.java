package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EnrollmentDemo {

    Connection getConnection() throws ClassNotFoundException, SQLException {
        // step 1: đăng ký jdbc
        Class.forName("org.mariadb.jdbc.Driver"); // trong dấu "" là tên class sử dụng làm driver (DriverManager)
        // step 2: url của db
        String url = "jdbc:mariadb://localhost:3306/jft"; // "jdbc:<tên loại sql>://<host>:<port>/<tên database>
        // step 3: tạo connection
        Connection connection = DriverManager.getConnection(url, "root", "root");
        // auto commit
        connection.setAutoCommit(false);
        return connection;
    }

    void createTable(Connection connection) throws SQLException {
        try {
            String statement = "create table testtable2 (id long primary key auto_increment, name varchar(255))";
//        connection.createStatement().execute(statement); // không lấy kết quả trả về/kết quả trả về không có nhiều ý nghĩa
            connection.createStatement().executeUpdate(statement); // executeUpdate sử dụng nhiều với các câu lệnh create/update/delete
            connection.commit(); // đồng ý thực thi các câu lệnh bên trên, sử dụng hàm này khi autoCommit mode là false, không sử dụng khi autoCommit  mode là true
        } catch (Exception e) {
            connection.rollback(); // undo, hoàn tác
        }
    }

    /**
     *
     * @param connection
     * @throws SQLException
     */
    void createTransaction(Connection connection) throws SQLException {
        try {
            // Một giao dịch ck ngân hàng từ acc1 sang acc2
            // Step 1: trừ 100k từ acc1
            // Exception: mất mạng,....
            // Step 2: cộng 100k từ acc2
        } catch (Exception e) {

        }
    }

    void selectTable(Connection connection) throws SQLException {
        ResultSet rs= null;
        String statement = "select * from testtable2" ;
//        Statement statementObj = connection.createStatement();
//        rs = statementObj.executeQuery(statement);
//        statementObj.close(); // tăng performance cho code

        // ? đại diện cho một param chưa biết trước, đến khi thực thi mới truyền vào
        // Performance của PreparedStatment cao hơn Statement
        PreparedStatement preparedStatementObj = connection.prepareStatement("select * from testtable2 WHERE id = ?");

        while (rs.next()){
//            System.out.println(rs.getInt(1));
            System.out.println(rs.getString("id"));
//            System.out.println(rs.getString(2));
            System.out.println(rs.getString("name"));
        }
        rs.close();
    }

    /**
     * Tạo table theo câu lệnh
     * @param connection
     * @param statement
     *  create table student(id bigint primary key, lastname varchar(50), firstname varchar(50), enrollment_date varchar(50))
     *  create table course(id bigint primary key, title varchar(50), credits int)
     *  create table enrollment(id bigint primary key, course_id bigint, student_id bigint)
     * @throws SQLException
     */
    void createTable(Connection connection, String statement) throws SQLException {
        try {
            connection.createStatement().executeUpdate(statement);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        }
    }

    public void insertData(Connection connection, String dataStr) throws SQLException {
        try {
            connection.createStatement().executeUpdate(dataStr);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        }
    }

    public void insertData(Connection connection, String... dataStr) throws SQLException {
        try {
            for (String str: dataStr) {
                connection.createStatement().executeUpdate(str);
            }
            connection.commit();
        } catch (Exception e) {
//            connection.commit(); // play with it
//            connection.rollback();
        }
    }
}
