package stackAndHeap;

/**
 * @author: nvankhoiws@gmail.com
 * @since: 23/10/2021
 */
public class StringImmutable {
    /**
     * immutable is constant
     */
    void stringImmutable(){
        String str = "We are the champion";
        str = "We are not the champion";
        System.out.println();
//        String str2 = "We are the champion";
    }
}
