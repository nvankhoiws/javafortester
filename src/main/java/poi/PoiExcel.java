package poi;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class PoiExcel {

    public void mapExample (){
        Map<String, String> aMap = new HashMap<>();
        // Cách đẩy/chứa phần tử trong map
        aMap.put("Hanoi", "danso: 1trieu, dientich: 320km2");
        aMap.put("HCM", "danso: 2trieu, dientich: 640km2");
        aMap.put("DaNang", "danso: 3trieu, dientich: 960km2");
        // Cách lấy phần tử từ map ra
        String dansoVaDienTichCuaHanoi = aMap.get("Hanoi");

        System.out.println(dansoVaDienTichCuaHanoi);

        boolean kiemTra = aMap.containsKey("CanTho");

        System.out.println("CanTho co trong map khong ?" + kiemTra);

        Set<String> tapHopCacThanhPho = aMap.keySet();

        for (String motThanhPho : tapHopCacThanhPho ) {
            System.out.println(motThanhPho);
        }
    }

    public Map<Integer, List<String>> readExcel(String fileLocation) throws IOException {
        // Khai báo file định đọc
        Map<Integer, List<String>> data = new HashMap<>(); // Integer chỉ cho row, List<String> tập hợp các cell của row
        FileInputStream file = new FileInputStream(new File(fileLocation));

        // Tạo một workbook từ file
        Workbook workbook = new XSSFWorkbook(file);

        // Khai báo một sheet
        Sheet sheet = workbook.getSheetAt(0);
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<String>());
            for (Cell cell : row) {
                switch (cell.getCellType()) {
                    case STRING:
                        data.get(i)
                                .add(cell.getRichStringCellValue()
                                        .getString());
                        break;
                    case NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            data.get(i)
                                    .add(cell.getDateCellValue() + "");
                        } else {
                            data.get(i)
                                    .add((int)cell.getNumericCellValue() + "");
                        }
                        break;
                    case BOOLEAN:
                        data.get(i)
                                .add(cell.getBooleanCellValue() + "");
                        break;
                    case FORMULA:
                        data.get(i)
                                .add(cell.getCellFormula() + "");
                        break;
                    default:
                        data.get(i)
                                .add(" ");
                }
            }
            i++;
        }
        if (workbook != null){
            workbook.close();
        }
        return data;
    }

    public void writeExcel() throws IOException {
        // Khai báo một work book
        Workbook workbook = new XSSFWorkbook();

        try {
            // Khai báo một work sheet
            Sheet sheet = workbook.createSheet("Persons");
            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 4000);

            // Khai báo header
            Row header = sheet.createRow(0);

            // Tạo một object style
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            // Tạo một object font
            XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setFontName("Tahoma");
            font.setFontHeightInPoints((short) 16);
            font.setBold(true);
            font.setItalic(true);

            // Set font cho header style
            headerStyle.setFont(font);

            // Khai báo các cell cho header
            Cell headerCell = header.createCell(0);
            headerCell.setCellValue("Tên");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(1);
            headerCell.setCellValue("Tuổi");
            headerCell.setCellStyle(headerStyle);

            // Style cho row chứa data
            CellStyle style = workbook.createCellStyle();
            style.setWrapText(true);

            Row row = sheet.createRow(2);
            Cell cell = row.createCell(0);
            cell.setCellValue("Cao Dung");
            cell.setCellStyle(style);

            cell = row.createCell(1);
            cell.setCellValue(50);
            cell.setCellStyle(style);

            // add code here

            // Ghi ra file
            // File này nằm ở đâu
            String fileLocation = System.getProperty("user.dir") +"/" + "temp.xlsx";

            FileOutputStream outputStream = new FileOutputStream(fileLocation);

            workbook.write(outputStream);
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
    }
}
