package inheritanceInJava;

public class WhiteDuck extends Duck implements Quackable, Flightable{

    @Override
    public String flight() {
         return "Flight";
    }

    @Override
    public String quack() {
        return "Quack";
    }
}
