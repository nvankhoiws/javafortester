package inheritanceInJava;

/**
 * Liên quan đến khả năng của đối tượng
 */
public interface Flightable {
    public String flight();
}
