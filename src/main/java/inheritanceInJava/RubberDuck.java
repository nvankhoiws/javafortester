package inheritanceInJava;

public class RubberDuck extends Duck implements Quackable {

    public String quack() {
        return  "Quezze";
    }
}
