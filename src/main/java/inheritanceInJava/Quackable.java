package inheritanceInJava;

/**
 * Inteface là Kế thừa khả năng qua các method
 */
public interface Quackable {
    public String quack();
}
