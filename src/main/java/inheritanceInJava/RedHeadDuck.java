package inheritanceInJava;

/**
 * @author: nvankhoiws@gmail.com
 * @since: 23/10/2021
 */
public class RedHeadDuck extends Duck implements Quackable, Flightable {
    @Override
    public String flight() {
        return "Flight";
    }

    @Override
    public String quack() {
         return "Quack";
    }
}
