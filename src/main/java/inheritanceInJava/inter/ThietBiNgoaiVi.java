package inheritanceInJava.inter;

public interface ThietBiNgoaiVi { // chuẩn chung, blueprint, footprint, template
//    private int canNang; // không thể sử dụng các biến

    public void hinhDang();

    public void doLonDongDien();

    private void mauSac() {

    }

    default void chatLieu(){
        mauSac();
    }

    // mặc định các hàm của interface là public
    // private và default trong interface ? Homework
    // Tại sao lại phải implement body của hàm private và default trong interface

}
