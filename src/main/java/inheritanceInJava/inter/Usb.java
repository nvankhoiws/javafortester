package inheritanceInJava.inter;

public class Usb implements ThietBiNgoaiVi, ThietBiDienTu {
    // PHẢI implement tất cả public methods của tất cả các interface
    // không nhất thiết phải Override default method, Override nếu muốn

    @Override
    public void hinhDang() {

    }

    @Override
    public void doLonDongDien() {

    }

    @Override
    public void hangSanXuat() {

    }
}
