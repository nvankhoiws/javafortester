package jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcDemoTest {

    @Test
    void testConnection() throws SQLException, ClassNotFoundException {
        JdbcDemo demo = new JdbcDemo();
        demo.getConnection();
    }

    @Test
    void testCreateTable() throws SQLException, ClassNotFoundException {
        JdbcDemo demo = new JdbcDemo();
        Connection connection = demo.getConnection();
        demo.createTable(connection);
    }

}
