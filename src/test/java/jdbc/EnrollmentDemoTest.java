package jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class EnrollmentDemoTest {

    @Test
    void testConnection() throws SQLException, ClassNotFoundException {
        EnrollmentDemo demo = new EnrollmentDemo();
        demo.getConnection();
    }

    @Test
    void testCreateTable() throws SQLException, ClassNotFoundException {
        String createTableStudent = "        create table student\n" +
                "                (\n" +
                "                        id bigint auto_increment,\n" +
                "                        lastname nvarchar(50) null,\n" +
                "                firstname nvarchar(50) null,\n" +
                "                enrollment_date varchar(50) null,\n" +
                "                constraint student_pk\n" +
                "        primary key (id)\n" +
                ")";
//        drop table enrollment;
//
//        drop table course;
//
//        drop table student;
        String createTableCourse= "create table course\n" +
                "(\n" +
                "\tid bigint null,\n" +
                "\ttitle nvarchar(50) not null,\n" +
                "\tcredits int not null,\n" +
                "\tconstraint course_pk\n" +
                "\t\tprimary key (id)\n" +
                ");";

        String createTableEnrollment= "create table enrollment\n" +
                "(\n" +
                "\tid bigint null,\n" +
                "\tcourse_id bigint null,\n" +
                "\tstudent_id bigint null,\n" +
                "\tgrade float null,\n" +
                "\tconstraint enrollment_pk\n" +
                "\t\tprimary key (id),\n" +
                "\tconstraint enrollment_course_id_fk\n" +
                "\t\tforeign key (course_id) references course (id),\n" +
                "\tconstraint enrollment_student_id_fk\n" +
                "\t\tforeign key (student_id) references student (id)\n" +
                ");\n";

        String dropForeignCourseId = "alter table enrollment drop foreign key enrollment_course_id_fk;";
        String alterTableCourse = "alter table course modify id bigint auto_increment";
        String altertableEnrollment = "alter table enrollment\n" +
                "\tadd constraint enrollment_course_id_fk\n" +
                "\t\tforeign key (course_id) references course (id);";

        EnrollmentDemo demo = new EnrollmentDemo();
        Connection connection = demo.getConnection();
        demo.createTable(connection, createTableStudent);
        demo.createTable(connection, createTableCourse);
        demo.createTable(connection, createTableEnrollment);
        demo.createTable(connection, dropForeignCourseId);
        demo.createTable(connection, alterTableCourse);
        demo.createTable(connection, altertableEnrollment);
    }

    @Test
    void testInsertData() throws SQLException, ClassNotFoundException {
        EnrollmentDemo demo = new EnrollmentDemo();
        Connection connection = demo.getConnection();
        String dataStr = "INSERT INTO student(firstname, lastname, enrollment_date) VALUES ('khoi', 'nguyen van', '2021/10/23')";
        demo.insertData(connection, dataStr);
        dataStr = "INSERT INTO course(title, credits) VALUES ('Math', 20)";
        demo.insertData(connection, dataStr);
        dataStr = "INSERT INTO enrollment(student_id, course_id) VALUES (1, 1)";
        demo.insertData(connection, dataStr);
    }

    @Test
    void testRollBack() throws SQLException, ClassNotFoundException {
        EnrollmentDemo demo = new EnrollmentDemo();
        Connection connection = demo.getConnection();
        String dataStr = "INSERT INTO student(firstname, lastname, enrollment_date) VALUES ('khoi', 'nguyen van', '2021/10/23')";
        String dataStr1 = "INSERT INTO course(title, credits) VALUES ('Math', 20)";
        String dataStr2 = "INSERT INTO enrollment(student_id, course_id) VALUES (1, 1)";
        demo.insertData(connection, dataStr, dataStr1, dataStr2);

//        List<String> stringList = new ArrayList<>();
//
//        List<Integer> integerList = new ArrayList<>();
//
//        List objList = new ArrayList<>();
//        objList.add("String");
//        objList.add(67);
//
//        stringList.add("String");
//        stringList.add(23);
//
//        List<>
    }

}
