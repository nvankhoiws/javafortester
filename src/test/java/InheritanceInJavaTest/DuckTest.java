package InheritanceInJavaTest;

import inheritanceInJava.RedHeadDuck;
import inheritanceInJava.RubberDuck;
import inheritanceInJava.WhiteDuck;
import jdbc.EnrollmentDemo;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author: nvankhoiws@gmail.com
 * @since: 23/10/2021
 */
public class DuckTest {
    @Test
    void mauLongTest(){
        WhiteDuck whiteDuck = new WhiteDuck();
        whiteDuck.getMauLong();
    }

    @Test
    void quackTest(){
        WhiteDuck whiteDuck = new WhiteDuck();
        whiteDuck.quack();
    }

    @Test
    void test(){
        WhiteDuck whiteDuck = new WhiteDuck();
        RubberDuck rubberDuck = new RubberDuck();
        RedHeadDuck redHeadDuck = new RedHeadDuck();

        assertEquals(true, whiteDuck.getMauLong().equals(rubberDuck.getMauLong()));
        assertEquals(true, whiteDuck.getMauLong().equals(redHeadDuck.getMauLong()));
        assertEquals("Trang", whiteDuck.getMauLong());

        assertEquals("Quack", whiteDuck.quack());
        assertEquals("Quezze",rubberDuck.quack());

    }

//    @Test
//    void testRollBack() throws SQLException, ClassNotFoundException {
//        List<String> stringList = new ArrayList<>();
//        List<Integer> integerList = new ArrayList<>();
//        List objList = new ArrayList<>();
//        objList.add("String");
//        objList.add(67);
//
//        stringList.add("String");
//        stringList.add(23);
//
//        List<RedHeadDuck>
//    }
}
