package finalexam;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {
    @Test
    void getNameTest(){
        String str = "Dung";
        Customer customer = new Customer(str);

        assertEquals("Dung", customer.getName());
    }

    @Test
    void isMemberTest(){
        String str = "Dung";
        Customer customer = new Customer(str);

        assertEquals(false, customer.isMember());
    }

    @Test
    void setMemberTest(){
        Customer customer = new Customer("Dung");
        customer.setMember(true);

        assertEquals(true, customer.isMember());
    }

    @Test
    void setGetMemberTypeTest(){
        Customer customer = new Customer("Dung");
        customer.setMember(true);
        customer.setMemberType("Premium");

        assertEquals("Premium", customer.getMemberType());
    }

    @Test
    void setMemberTypeTest(){
        Customer customer = new Customer("Dung");
        customer.setMember(true);

        assertEquals(true, customer.getMemberType().equals("Premium")
                || customer.getMemberType().equals("Gold")
                || customer.getMemberType().equals("Silver"));
    }

}
