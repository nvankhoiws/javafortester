package finalexam;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DiscountRateTest {
    @Test
    void serviceDiscountPremiumTest () { // tất cả method test đều phải return void
        // expected là kết quả mong muốn, actual là kết quả code trả về
//        assertEquals(0.2, new DiscountRate().serviceDiscountPremium);
        assertEquals(0.2, DiscountRate.serviceDiscountPremium);
    }

    @Test
    void serviceDiscountGoldTest () {
        assertEquals(0.15, DiscountRate.serviceDiscountGold);
    }

    @Test
    void serviceDiscountSilverTest () {
        assertEquals(0.1, DiscountRate.serviceDiscountSilver);
    }

    @Test
    void productDiscountPremiumTest () {
        assertEquals(0.1, DiscountRate.productDiscountPremium);
    }
}
