package finalexam;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VisitTest {
    @Test
    void getServiceExpenseTest(){
        Visit tinhVisit = new Visit("Tinh", Date.from(Instant.now()));
        tinhVisit.setServiceExpense(30);

        assertEquals(30, tinhVisit.getServiceExpense());

        tinhVisit.setServiceExpense(70);

        assertEquals(100, tinhVisit.getServiceExpense());

        tinhVisit.getCustomer().setMemberType("Premium");

        assertEquals(80, tinhVisit.getServiceExpense());

        tinhVisit.getCustomer().setMemberType("Gold");

        assertEquals(85, tinhVisit.getServiceExpense());

        tinhVisit.getCustomer().setMemberType("Silver");

        assertEquals(90, tinhVisit.getServiceExpense());
    }

    @Test
    void getProductExpenseTest(){
        Visit tinhVisit = new Visit("Tinh", Date.from(Instant.now()));
        tinhVisit.setProductExpense(30);

        assertEquals(30, tinhVisit.getProductExpense());

        tinhVisit.setServiceExpense(70);

        assertEquals(100, tinhVisit.getProductExpense());

        tinhVisit.getCustomer().setMemberType("Silver");

        assertEquals(90, tinhVisit.getProductExpense());
    }

    @Test
    void getTotalExpenseTest(){
        Visit tinhVisit = new Visit("Tinh", Date.from(Instant.now()));
        tinhVisit.setProductExpense(30);
        tinhVisit.setServiceExpense(50);

        assertEquals(80, tinhVisit.getTotalExpense());

        tinhVisit.getCustomer().setMemberType("Gold");

        assertEquals(0.85*50 + 0.9*30, tinhVisit.getTotalExpense());
    }

}
