package exceptionInJava;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author: nvankhoiws@gmail.com
 * @since: 23/10/2021
 */
public class ThrowExceptionDemoTest {
    @Test
    void throwExceptionDemoTest() throws IOException, FileNotFoundException {
        ThrowExceptionDemo throwExceptionDemo = new ThrowExceptionDemo();
        throwExceptionDemo.downloadFile(); // IOException
        throwExceptionDemo.writeFile(); // FileNotFoundException
    }

    @Test
    void throwExceptionDemoTest2() {
        ThrowExceptionDemo throwExceptionDemo = new ThrowExceptionDemo();
        try {
            throwExceptionDemo.downloadFile(); // IOException
            throwExceptionDemo.writeFile(); // FileNotFoundException
        } catch (Exception e) {
            System.out.println("Tôi không có lỗi");
        }
    }

    @Test
    void throwExceptionDemoTest3() {
        ThrowExceptionDemo throwExceptionDemo = new ThrowExceptionDemo();
        try {
            throwExceptionDemo.downloadFile(); // IOException
            throwExceptionDemo.writeFile(); // FileNotFoundException
        } catch (FileNotFoundException e) {
            System.out.println("Tôi không có lỗi FileNotFoundException");
        } catch (IOException e) {
            System.out.println("Tôi không có lỗi IOException");
        }
        // Maybe có khả năng phải code 100... 1000 loại exception handling khác nhau
    }

    @Test
    void throwExceptionDemoTest4() throws ClassNotFoundException {
        ThrowExceptionDemo throwExceptionDemo = new ThrowExceptionDemo();
        try {
            throwExceptionDemo.downloadFile(); // IOException
            throwExceptionDemo.writeFile(); // FileNotFoundException
        } catch (IOException e) {
            throw new ClassNotFoundException(); // customized exception
        }
    }
}
