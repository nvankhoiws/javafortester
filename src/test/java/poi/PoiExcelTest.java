package poi;

import org.junit.jupiter.api.Test;

import java.io.IOException;

class PoiExcelTest {

    @Test
    void writeExcel() throws IOException {
        PoiExcel poiExcel = new PoiExcel();
        poiExcel.writeExcel();
    }

    @Test
    void mapTest() throws IOException {
        PoiExcel poiExcel = new PoiExcel();
        poiExcel.mapExample();
    }


    @Test
    void readExcelTest() throws IOException {
        PoiExcel poiExcel = new PoiExcel();
        poiExcel.readExcel(System.getProperty("user.dir") + "/" + "temp.xlsx");
    }

}